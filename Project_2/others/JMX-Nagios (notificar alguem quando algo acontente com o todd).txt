Notificar alguem quando algo acontence com a aplicação

Para que isto seja possivel tivemos que criar um event-handler, no qual é verificado vários estados de variaveis.

#!/bin/bash

SERVICESTATE=$1
LASTSERVICESTATE=$2
NOTIFICATIONTYPE=$3
SERVICEDESC=$4
HOSTALIAS=$5
HOSTADDRESS=$6
LONGDATETIME=$7
SERVICEOUTPUT=$8
SERVICENOTESURL=$9
CONTACTEMAIL=${10}
CONTACTPAGER=${11}
TIME=${12}

send_email() {
	sendemail -f "cogsi2009@live.com" -t "cogsi2009@live.com" -u "subject" -m "message" -s smtp.live.com:587 -xu cogsi2009@live.com -xp 
    #/usr/bin/printf "%b" "***** Nagios *****\n\nNotification Type: $NOTIFICATIONTYPE\n\nService: $SERVICEDESC\nHost: $HOSTALIAS\nAddress: $HOSTADDRESS\nState: #$SERVICESTATE\n\nDate/Time: $LONGDATETIME\n\nAdditional Info: $SERVICEOUTPUT\n\nURL: $SERVICENOTESURL" | /bin/mail -s "** $NOTIFICATIONTYPE Service Alert: #$HOSTALIAS/$SERVICEDESC is $SERVICESTATE **" $CONTACTEMAIL
}

send_sms() {
    /usr/bin/wget --user=notifier --password=x "http://ip:port/smsgate/sms?tos=$CONTACTPAGER&content=$NOTIFICATIONTYPE, $SERVICEDESC, $HOSTADDRESS, $SERVICESTATE, $TIME, $SERVICEOUTPUT"

}

if [ $NOTIFICATIONTYPE = "PROBLEM" ]; then
    if [ $SERVICESTATE = "WARNING" ]; then
        send_email
    elif [ $SERVICESTATE = "CRITICAL" ]; then
        send_email
        send_sms
    fi
elif [ $NOTIFICATIONTYPE = "RECOVERY" ]; then
    if [ $LASTSERVICESTATE = "WARNING" ]; then
        send_email
    elif [ $LASTSERVICESTATE = "CRITICAL" ]; then
        send_email
        send_sms
    fi
fi

-----------------------------------------------------------------------------------------------

define command { 
    command_name      sendemail
    command_line      sendemail -f "cogsi2009@live.com" -t "cogsi2009@live.com" -u "subject" -m "message" -s smtp.live.com:587 -xu cogsi2009@live.com -xp 2009cogsi -o tls=yes

}

-----------------------------------------------------------------------------------------------

define command{
    command_name    notify-service-todd
    command_line    $USER1$/notify.sh $SERVICESTATE$ $LASTSERVICESTATE$ $NOTIFICATIONTYPE$ $SERVICEDESC$ $HOSTALIAS$ $HOSTADDRESS$ "$LONGDATETIME$" "$SERVICEOUTPUT$" "$SERVICENOTESURL$" $CONTACTEMAIL$ $CONTACTPAGER$ $TIME$
}

-----------------------------------------------------------------------------------------------

define service{
 use local-service
 hostname localhost
 service_description servico notify TODD
 event_handler notify-service-todd
}

-----------------------------------------------------------------------------------------------
	
*************************************************************************************************************************************************
templates.cfg
*************************************************************************************************************************************************

define contact{
        name                            single-contact
        service_notification_period     24x7
        host_notification_period        24x7
        service_notification_options    w,u,c,r,f,s
        host_notification_options       d,u,r,f,s
        service_notification_commands   notify-service-todd
        host_notification_commands      sendemail
        register                        0
}